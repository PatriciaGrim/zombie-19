const population = [
    {
        name: "Pango Lin",
        age: 25,
        infected: false,
        variant: null,
        immune: false,
        dead: false,
        canTransmit: true,
        friends: [
            {
                name: "Eric Zemmour",
                age: 19,
                infected: false,
                variant: null,
                immune: false,
                dead: false,
                canTransmit: true,
                friends: []
            },
            {
                name: "Emma Gination",
                age: 29,
                infected: false,
                variant: null,
                immune: false,
                dead: false,
                canTransmit: true,
                friends: [
                    {
                        name: "Max Imum",
                        age: 99,
                        infected: true,
                        variant: "Zombie-B",
                        immune: false,
                        dead: false,
                        canTransmit: true,
                        friends: []
                    },
                    {
                        name: "Axel Ration",
                        age: 4,
                        infected: false,
                        variant: null,
                        immune: false,
                        dead: false,
                        canTransmit: true,
                        friends: []
                    },
                    {
                        name: "Tara Tata",
                        age: 38,
                        infected: false,
                        variant: null,
                        immune: false,
                        dead: false,
                        canTransmit: true,
                        friends: []
                    },
                    {
                        name: "Pascal Obispo",
                        age: 55,
                        infected: false,
                        variant: null,
                        immune: false,
                        dead: false,
                        canTransmit: true,
                        friends: []
                    }
                ]
            },
        ],
    },
    {
        name: "Ali Gator",
        age: 40,
        infected: false,
        variant: null,
        immune: false,
        dead: false,
        canTransmit: true,
        friends: []
    },
    {
        name: "Odile Croc",
        age: 46,
        infected: false,
        variant: null,
        immune: false,
        dead: false,
        canTransmit: true,
        friends: [
            {
                name: "Booba",
                age: 51,
                infected: false,
                variant: null,
                immune: false,
                dead: false,
                canTransmit: true,
                friends: []
            },
            {
                name: "Ray Manta",
                age: 23,
                infected: false,
                variant: null,
                immune: false,
                dead: false,
                canTransmit: true,
                friends: [
                    {
                        name: "Marc Assin",
                        age: 2,
                        infected: false,
                        variant: null,
                        immune: false,
                        dead: false,
                        canTransmit: true,
                        friends: []
                    }
                ]
            },
        ]
    },
];

// Zombie-A : Infecte du haut vers le bas.
function infectA(person, min_age=null) {
    person.friends.forEach((friend) => {
        if(!friend.immune && person.canTransmit) {
            friend.infected = min_age ? friend.age >= min_age : true;
            friend.variant = person.variant;
        }
        infectA(friend);
    });
}

// Zombie-B : Infecte du bas vers le haut
function infectB(person, min_age=null) {
    let parents = min_age ? findParents(population, person).filter(parent => parent.age >= min_age): findParents(population, person);
    parents.forEach((parent) => {
        if(!parent.immune && person.canTransmit) {
            parent.infected = true;
            parent.variant = person.variant;
        }
    });
}

// Zombie-32 : Infecte du bas vers le haut et du haut vers le bas toutes personnes qui a 32 ans et plus
function infect32(person) {
    infectA(person, 32);
    infectB(person, 32);
}

// Zombie-C : Infecte une personne sur 2 dans un groupe social (mais pas les groupes sociaux en contact)
function infectC(person) {
    let parent = findParents(population, person).pop();
    if (parent) {
        for (let i = 0; i < parent.friends.length; i++) {
            let isIndexEven = parent.friends.findIndex(friend => friend == person) % 2 === 0;
            if (!parent.friends[i].immune && person.canTransmit) {
                parent.friends[i].infected = isIndexEven ? i % 2 == 0 : (i + 1) % 2 == 0;
                parent.variant = person.variant;
            }
        }
    }
}

// Zombie-Ultime : Infecte seulement la personne racine la plus ascendante
function infectUltime(person) {
    let higher_parent = findParents(population, person)[0];
    if(higher_parent && !higher_parent.immune && person.canTransmit) { 
        higher_parent.infected = true;
        higher_parent.variant = person.variant;
    } 
}

// Retrouver toutes les personnes infectées de l'arbre passé en paramètre
function findInfected(population) {
    let infectedPopulation = [];
    for (let person of population) {
        if (person.infected) {
            infectedPopulation.push(person);
        }
        if (person.friends.length > 0) {
            let friendsInfected = findInfected(person.friends);
            infectedPopulation = infectedPopulation.concat(friendsInfected);
        }
    }
    return infectedPopulation;
}

// Retrouver tous les ascendants d'un élément target dans l'arbre population 
function findParents(population, target, parents = []) {
    for (let i = 0; i < population.length; i++) {
        const person = population[i];
        if (person == target) {
            return parents;
        } else if (person.friends && person.friends.length > 0) {
            const nestedParents = findParents(person.friends, target, [...parents, person]);
            if (nestedParents.length > 0) {
                return nestedParents;
            }
        }
    }
    return [];
}

// Vaccin-A.1 contre Zombie-A et Zombie-32 : N’est pas encore très efficace il permet de soigner toutes les personnes d’un âge compris entre 0 ans et 30 ans et de les immuniser contre tous les variants (Ascendant et Descendant)
function vaccinateA1(person) {
    if((person.variant == "Zombie-A" ||  person.variant == "Zombie-A") && person.age <= 30) {
        person.infected = false;
        person.immune = true;
        vaccinateA1Descending(person);
        vaccinateA1Ascending(person);
    }
}

function vaccinateA1Descending(person) {
    person.friends.forEach((friend) => {
        if (friend.infected && friend.age <= 30) {
            if (friend.variant === "Zombie-A" || friend.variant === "Zombie-32") {
                friend.infected = false;
                friend.immune = true;
                vaccinateA1Descending(friend);
            }   
        }
    });
}

function vaccinateA1Ascending(person) {
    let parents = findParents(population, person).filter(parent => parent.age <= 30);
    parents.forEach((parent) => {
        if (parent.infected) {
            if (parent.variant === "Zombie-A" || parent.variant === "Zombie-32") {
                parent.infected = false;
                parent.immune = true;
                vaccinateA1Ascending(friend);
            }   
        }
    });
}

// Vaccin-B.1 contre Zombie-B et Zombie-C : Il tue une personne sur 2 et soigne les autres mais ne leur donne pas l’immunité. (Ascendant et Descendant)
function vaccinateB1(person, i) {
    if(person.variant == "Zombie-B" ||  person.variant == "Zombie-C") {
        if (i % 2 === 0) {
            person.infected == false;
        } else {
            person.dead = true;
        }
        vaccinateB1Descending(person, i + 1);
        vaccinateB1Ascending(person, i + 1);
    }
}

function saveOrKill(person, i) {
    i % 2 === 0 ? person.infected = false : person.dead = true;
}

function vaccinateB1Descending(person) {
    person.friends.forEach((friend, i) => {
        if (friend.infected && (friend.variant === "Zombie-B" || friend.variant === "Zombie-C")) {
            saveOrKill(friend, i)
            vaccinateB1Descending(friend);
        }
    });
}

function vaccinateB1Ascending(person) {
    let parents = findParents(population, person);
    parents.forEach((parent, i) => {
        if (parent.infected && (parent.variant === "Zombie-B" || parent.variant === "Zombie-C")) {
            saveOrKill(parent, i)
            vaccinateB1Ascending(parent);
        }   
    });
}

// Vaccin-Ultime contre Zombie-Ultime : Son porteur ne pourra plus jamais être infecté et infecter les autres.
function vaccinateUltime(person) {
    if(person.variant == "Zombie-Ultime") {
        person.canTransmit = false;
        person.immune = true;
        person.infected = false;
    }
}

// Fonction supérieure permettant de répandre un variant ou un vaccin au sein de l'arbre population
function expand(population, callback) {
    const infected = findInfected(population)
    for (let i = 0; i < infected.length; i++) {
        callback(infected[i], i);
    }
    return population;
}

// Formattage de la sortie pour une lecture plus claire à l'aide de symboles.
function display(data, level = 0) {
    for (let i = 0; i < data.length; i++) {
        let indentation = " ".repeat(level * 6);
        console.log(indentation + "> " + data[i].name + " (" + data[i].age + " ans)" + (data[i].variant ? ' - ' + data[i].variant : '') + " "  + (data[i].infected ? "🔴" : "🟢") + " "  + (data[i].dead ? "💀" : "") + " "  + (!data[i].canTransmit ? "🔒" : ""));
        if (data[i].friends.length > 0) {
            display(data[i].friends, level + 1);
        }
    }
}

console.log("Legend:\n🔴 Infected\n🟢 Not infected\n💀 Dead\n🔒 Can't transmit\n\n")

console.warn("\nHere is an initial state of the population, when only patient zero was infected...\n\n");
display(population);

console.warn("\nNow, make way for the apocalypse!\nThe Zombie-19 pandemic is starting to take its toll.\n\n");
let contaminate = expand(population, infectB);
display(contaminate);

console.warn("\nIt's now time to vaccine people, that will hopefully give you a clear conscience.\n\n");
let vaccinate = expand([...contaminate], vaccinateB1);
display(vaccinate);